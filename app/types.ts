export type Player = {
    name:string,
    score:number,
    canBuzz:boolean
}
export type Game = {
    id: number,
    players: Player[],
    status: "created" | "started" | "ended"
}

export type GameStatus = {
    msg:string,
    type:'info' | 'error' | 'success'
}