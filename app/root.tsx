import stylesheet from './app.css'
import type { LinksFunction } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";

export const links: LinksFunction = () => [
  { rel:"preconnect", href:"https://fonts.googleapis.com"},
  { rel:"preconnect", href:"https://fonts.gstatic.com", crossOrigin:"anonymous"},
  { rel:"stylesheet", href:"https://fonts.googleapis.com/css2?family=ABeeZee&display=swap"},
  { rel: "stylesheet", href: stylesheet }
];

export default function App() {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body className="flex justify-center p-6 flex-col w-full md:w-1/2 max-w-[500px] align-center items-center m-auto space-y-4">
        <Outlet/>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
