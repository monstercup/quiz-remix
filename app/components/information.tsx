import clsx from "clsx";
import { ReactNode } from "react";

interface Props {
    className?: string;
    type: "error" | "info" | "success";
    children: ReactNode;
  }

export default function Information({className="", type="info", children}: Props) {
    return(
        <span
        className={clsx(
            className,
            'rounded-md border p-4 text-center',
            type === "info" && 'border-amber-800 text-amber-800 bg-amber-100',
            type === "error" && 'border-red-800 text-red-800 bg-red-100',
            type === "success" && 'border-green-800 text-green-800 bg-green-100',
        )}>
            {children}
        </span>
    )
}