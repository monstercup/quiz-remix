import { Link } from "@remix-run/react";
import clsx from "clsx";
import { MouseEventHandler, ReactNode } from "react";

interface Props {
    className?: string;
    type?: "button" | "submit" | "reset" | undefined;
    to?: string;
    target?: string;
    disabled?: boolean;
    destructive?: boolean;
    children: ReactNode;
    onClick?: MouseEventHandler<HTMLButtonElement>;
  }

export default function Button({className="", type="button", onClick, disabled, to, children}: Props) {
    let buttonClasses = 'bg-black text-white px-4 py-3 text-center rounded-lg'
    if(to){
        return(
            <Link 
                to={to}
                className={clsx(className, buttonClasses, disabled && "cursor-not-allowed opacity-80 saturate-50")}
            >
                {children}
            </Link>
        )
    }
    else return(
        <button
            onClick={onClick}
            className={clsx(className, buttonClasses, disabled && "cursor-not-allowed opacity-70 saturate-50")}
            type={type}
            disabled={disabled}
        >
            {children}
        </button>
    )
}