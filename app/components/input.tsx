import clsx from "clsx";
import { forwardRef, ReactNode, Ref, RefObject, useImperativeHandle, useRef } from "react";

export interface RefInputText {
  input: RefObject<HTMLInputElement> | RefObject<HTMLTextAreaElement>;
}

interface Props {
  name: string;
  placeholder?: string;
  label?:string,
  value?: string;
  setValue?: React.Dispatch<React.SetStateAction<string>>;
  className?: string;
  minLength?: number;
  maxLength?: number;
  disabled?: boolean;
  readOnly?:boolean;
  required?: boolean;
  autoComplete?: string;
  type?: string;
}
const Input = (
  {
    name,
    placeholder,
    label,
    value,
    setValue,
    className,
    disabled = false,
    readOnly =false,
    required = false,
    minLength,
    maxLength,
    autoComplete,
    type = "text",
  }: Props,
  ref: Ref<RefInputText>
) => {
  useImperativeHandle(ref, () => ({ input }));
  const input = useRef<HTMLInputElement>(null);

  function onChange(e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
    if (setValue) {
        setValue(e.currentTarget.value);
    }
  }

  return (
    <div className={clsx(className, 'mb-2')}>
      {label && (
        <label htmlFor={name} className="flex justify-between space-x-2 text-xs font-medium text-gray-600 truncate">
          <div className=" flex space-x-1 items-center">
            <div>
              {label}
              {required && <span className="ml-1 text-red-500">*</span>}
            </div>
          </div>
        </label>
      )}
      <div className={clsx("flex rounded-md shadow-sm w-full relative", label && "mt-1")}>
            <input
              ref={input}
              type={type}
              id={name}
              name={name}
              autoComplete={autoComplete}
              required={required}
              minLength={minLength}
              maxLength={maxLength}
              defaultValue={value ?? ""}
              onChange={onChange}
              disabled={disabled}
              readOnly={readOnly}
              placeholder={placeholder}
              className={clsx(
                "w-full p-2 border border-gray-500 flex-1 focus:ring-accent-500 focus:border-accent-500 block min-w-0 rounded-md sm:text-sm",
                className,
                (disabled || readOnly) && "bg-gray-100 cursor-not-allowed"
              )}
            />
      </div>
    </div>
  );
};
export default forwardRef(Input);