import axios from 'axios';

export const apiRequest = (url:string, method?:string, data?:any) => {
  return new Promise((resolve, reject) => {
    // if (!url.startsWith('http')) url = api.domain + url
    axios({
        headers: {'Content-Type': 'application/json', Accept: 'application/json'} ,
        method: method ? method : "GET",
        url: url,
        ...(method && method !== "GET" && method !=="DELETE" && data && {data:JSON.stringify(data)})
    })
      .then(function (response:any) {
        resolve(response.data)
      })
      .catch(function (error:any) {
        if (error.response) {
          reject(error.response.data)
          return
        }
        reject(error)
      })
  })
}
