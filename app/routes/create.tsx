import { useEffect, useState } from "react";
import { socket } from "~/socket";
import { Game } from '../types'
import { apiRequest } from "~/api";
import { create } from "node_modules/axios/index.cjs";
import { randomGameId } from "~/utils";
import { useNavigate } from "@remix-run/react";
import Button from "~/components/button";
import clsx from "clsx";
import Information from "~/components/information";

export default function Create() {
    const navigate = useNavigate();
    const [game, setGame] = useState<Game>()
    const [gameStarted, setGameStarted] = useState<string>()

    const startGame = () => {
        socket.emit('start-game')
        // afficher message de commencement de la partie puis rediriger vers l'admin après 5 secondes
    }

    const cancelGame = (origin?:string) => {
        socket.emit('cancel-game')
        sessionStorage.removeItem("current_game")
        navigate('/')
    }

    const createGame = () => {
        sessionStorage.removeItem("current_game")
        let game : Game = {id: randomGameId(), players:[], status:"created" }
        let url = "http://localhost:4000/game/new"
        apiRequest(url, "POST", game).then((res:any) => {
            socket.emit('create-game', res)
            setGame(res)
            sessionStorage.setItem("current_game", JSON.stringify(res));
        })
    }

    const getGame = (id:string) => {
        const url = "http://localhost:4000/game/" + id
        return apiRequest(url, "GET").then((res:any) => {
            setGame(res)
            socket.emit('join-game', res)
        }).catch(() => createGame())
    }

    useEffect(() => {
        const game = sessionStorage.getItem("current_game")
        const current_game = game && JSON.parse(game)
        if (current_game && current_game.id) getGame(current_game.id)
        else createGame()
    }, [])

    useEffect(() => {
        socket.on("start", () => {
            // sessionStorage.setItem("current_game", JSON.stringify(game))
            setGameStarted('La partie va bientôt commencer !')
            setTimeout(()=> {
                navigate('/admin')
             }
             ,5000);
            // afficher message de commencement de la partie puis rediriger vers l'admin après 5 secondes
        })
        socket.on("game-update", (game) => {
            //player join emit ? 
            setGame(game)
            sessionStorage.setItem('current_game', JSON.stringify(game))
        })
        socket.on('cancelled', () => cancelGame("socket"))
        return () => {
            socket.off('cancel-game');
          };
    }, [socket])
    
    return (
      <>
        <h1 className="text-2xl text-center">Identifiant de la partie</h1>
        <b className="text-6xl font-bold text-center">{game && game.id}</b>
        
        {game && game.players && 
        (<> <h2 className="text-center font-bold">
            {game.players.length 
                ? game.players.length + " joueur" + ((game.players.length > 1 ? "s" : '') + " connecté" + ((game.players.length > 1 ? "s" : "")))
                : "En attente de joueur"
            }
            </h2>
            <ul className="w-full">
                {game.players.map((v,k) => 
                    <li className={clsx(k % 2 === 0 ? "bg-gray-200" : "bg-gray-300", "p-2")} key={k}>{v.name}</li>
                )}
            </ul>
 
         </>
        )
        
        }
        {gameStarted && <Information type="success">{gameStarted}</Information>}

        <Button type="button" className="bg-blue-500 hover:bg-blue-700" disabled={game && game.players.length < 1 && gameStarted !== null} onClick={() => startGame()}>Démarrer la partie</Button>
        <Button type="button" className="scale-75 bg-red-500 hover:bg-red-700" onClick={() => cancelGame()}>Annuler la partie</Button>
      </>
    );
  }
  