import { ActionFunctionArgs } from "@remix-run/node";
import { Form, Link, useNavigate } from "@remix-run/react";
import { useEffect, useRef, useState } from "react";
import { apiRequest } from "~/api";
import Button from "~/components/button";
import Information from "~/components/information";
import Input from "~/components/input";
import { socket } from "~/socket";
import { Game, Player, GameStatus } from "~/types";


export default function Join() {
    const [playerName, setPlayerName] = useState<string>('')
    const [gameId, setGameId] = useState<string>('')
    const [player, setPlayer] = useState<Player>()
    const [gameStatus, setGameStatus] = useState<GameStatus | null>()
    const navigate = useNavigate()
    
    const joinGame = () =>{
        let body = {name :playerName, gameId: gameId}
        let url = "http://localhost:4000/join-game"
        sessionStorage.setItem("playerName", playerName);
        apiRequest(url, "POST", body).then((res:any) => {
            setGameStatus({msg:"En attente de démarrage de la partie", type:"info"})
            socket.emit('join-game', res)
        })
    }

    const leaveGame = () => {
        let body = {name:playerName, gameId: gameId}
        let url = "http://localhost:4000/leave-game"
        apiRequest(url, "POST", body).then((res:any) => {
            setGameStatus(null)
            sessionStorage.removeItem('gameId')
            setGameId('')
            socket.emit('leave-game', res)
            
        })
    }

    const getGame = () => {
        let id = sessionStorage.getItem('gameId')
        if(id){
            const url = "http://localhost:4000/game/" + id
            return apiRequest(url, "GET").then((res:any) => {
                if(res satisfies Game) {
                    if (res.players.find((p:Player) => p.name === playerName)){
                        const player = res.players.find((p:Player) => p.name === playerName)
                        setGameId(res.id)
                        setPlayer(player)
                        setPlayerName(player.name) 
                        socket.emit('join-game', res)
                        setGameStatus({msg:"En attente de démarrage de la partie", type:"info"})
                    }
                    else{
                        sessionStorage.removeItem(('gameId'))
                    }                
                }
            })
        }
    }


    useEffect(() => {
        socket.on("start", () => {
            setGameStatus({msg:"La partie va bientôt commencer !", type:"success"})
            if(sessionStorage.getItem("playerName")){
                // sessionStorage.setItem("player",JSON.stringify({name:sessionStorage.getItem("playerName"), score:0, canBuzz:true}))
                setTimeout(()=> {
                    navigate('/buzzer')
                    // sessionStorage.removeItem("player_profile")
                 }
                 ,5000);
            }

        })

        socket.on('cancelled', () => {
            setGameStatus({msg:"Cette partie a été annulée. Vous pouvez en rejoindre une autre.", type:"error"})
            setGameId('')
        })

        socket.on('end', (game:Game) => {
            sessionStorage.setItem("current_game", JSON.stringify(game))
            navigate('/score')
        })
 
    }, [socket])


    useEffect(() => {
        getGame()
    }, [])

    useEffect(() => {
        sessionStorage.setItem('playerName', playerName)
        sessionStorage.setItem("gameId", gameId);
    }, [playerName, gameId])
    
    return (
      <>
        <h1 className="text-2xl font-bold text-center">Rejoignez une partie</h1>
        <Form id="join-game" method="post" className="flex flex-col w-4/5">
            <Input 
                name="playerName"
                label="Nom de joueur"
                value={playerName}
                setValue={setPlayerName}
            />
            <Input 
                name="gameId"
                label="Identifiant de la partie"
                value={gameId}
                setValue={setGameId}
            />
            <Button onClick={() => {gameId !== ''&& gameStatus ? leaveGame() : joinGame()}}>{gameId && gameStatus && gameStatus.type !== "error" ? "Quitter la partie" : "Rejoindre la partie"}</Button>
        </Form>
        {gameStatus && <Information className="w-4/5" type={gameStatus.type}>{gameStatus.msg}</Information>}

        <Link to={'/'} className="text-center underline hover:text-blue-600">Retour à l'accueil</Link>
      </>
    );
  }
  