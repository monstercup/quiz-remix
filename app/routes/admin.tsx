import { useNavigate } from "@remix-run/react"
import { useEffect, useState } from "react"
import { apiRequest } from "~/api"
import Button from "~/components/button"
import { socket } from "~/socket"
import { Game, Player } from "~/types"

export default function Admin() {
    const [game, setGame] = useState<Game>()
    const [queue, setQueue] = useState<Player[]>([])
    const [activePlayer, setActivePlayer] = useState<Player | null>(null)
    const navigate = useNavigate()

    const updateQueue = (player:Player) => {
        if(!queue.find(p => p.name === player.name)){
            setQueue([...queue, player])
        }
    }
    const updateScore = (player:Player, increment:boolean) => {
        // update api
        const updated_score = player.score + (increment ? 1 : -1)
        const updated_player = {name:player.name, score:updated_score, canBuzz:false }
        setActivePlayer(updated_player)
        socket.emit("update_score", updated_player, game)
        // if(game){
        //     const url = "http://localhost:4000/update-score/" + game.id
        //     return apiRequest(url, "POST", updated_player)
        // }

    }

    const updateActivePlayer = () => {
        let updated_queue = queue.filter(p => p.name !== activePlayer?.name)
        socket.emit("queue", updated_queue)
        setQueue(updated_queue)
        if (queue.length) setActivePlayer(queue[0])
        else setActivePlayer(null)
    }

    const resetBuzzers = () => {
        // update api
        if(game){
            setQueue([])
            setActivePlayer(null)
            socket.emit('reset_buzzers', game)
            socket.emit("queue", [])
        }
    }

    const getGame = () => {
        let g = sessionStorage.getItem('current_game')
        if(g){
            const url = "http://localhost:4000/game/" + JSON.parse(g).id
            return apiRequest(url, "GET").then((res:any) => {
                setGame(res)
                socket.emit('start_game', res?.id)
            }).catch(() => navigate('/create'))
        }
        else navigate('/create')

    }

    const endGame = () => {
        socket.emit('end-game', game)
        sessionStorage.setItem("current_game", JSON.stringify(game))
        navigate('/score')
        // update api
        // redirection score final
    }

    useEffect(() => {
        getGame()
    }, [])

    useEffect(() => {
        updateActivePlayer()
        socket.emit('queue', queue)
    }, [queue.length])

    useEffect(() => {
        socket.on('new-buzz', (player:Player) => {
            console.log(player, "hasbuzzed")
            updateQueue(player)
        })
        socket.on("game-update", (game) => {
            //player join emit ? 
            setGame(game)
            sessionStorage.setItem('current_game', JSON.stringify(game))
        })
    }, [socket])

    useEffect(() => {
        if(game){
            let sort_players = game.players.sort(({score:a}, {score:b}) => b-a)
            setGame({id:game.id, players:sort_players,status:game.status})
        }
    }, [game?.players])

    return(
        <>
            {queue.length && activePlayer
            ? 
                <>
                    <h1 className="text-3xl font-bold text-center flex flex-col"><span className="text-base font-normal">Au tour de </span>{activePlayer.name}</h1>
                    <div className="flex space-x-2 !mb-5 justify-center">
                        <Button className="bg-red-600 hover:bg-red-700 text-2xl" onClick={() => updateScore(activePlayer, false)}>-1</Button>
                        <p className="text-3xl font-bold flex flex-col items-center">{activePlayer.score} <span className="text-xl">points</span></p>
                        <Button className="bg-green-600 hover:bg-green-700 text-2xl" onClick={() => updateScore(activePlayer, true)}>+1</Button>
                    </div>
       
                    <Button className="bg-orange-500 hover:bg-orange-700" onClick={() => updateActivePlayer()}>Passer la main</Button>
                </>

            : <h1 className="text-3xl font-bold text-center">Posez votre question !</h1>}
            <h2 className="text-xl font-bold">File d'attente</h2>
            <ul className="w-full">
            {game && game.players && game.players.map((v,k) => (
                <li key={k} className={"p-2 flex justify-between items-center " + (k % 2 === 0 ? "bg-gray-200" : "bg-gray-300") +  (activePlayer?.name === v.name ? " border border-black" : (v.canBuzz ? "" : " opacity-80"))}>
                <section className="flex space-x-2">
                    <span className="font-bold text-lg">{v.name}</span>
                    {activePlayer?.name === v.name && <span className="bg-white text-xs rounded-lg px-2 flex align-center items-center">Buzz en cours</span>}
                    {activePlayer?.name !== v.name && queue.find(p => p.name === v.name) && <span className="bg-white text-xs rounded-lg px-2 flex align-center items-center">A buzzé</span>}
                </section>
                {v.score + " points"}
                </li>
            ))}
            </ul>

            <Button className="bg-blue-500 hover:bg-blue-700" onClick={() => resetBuzzers()}>Question suivante</Button> <legend className="text-xs text-gray-500 !mt-2">Ce bouton réinitialise les buzzers</legend>
            <Button className="bg-red-500 hover:bg-red-700" onClick={() => endGame()}>Terminer la partie</Button>
        </>
    )
}
