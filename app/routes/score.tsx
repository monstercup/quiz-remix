import { Link, useNavigate } from "@remix-run/react";
import { useEffect, useState } from "react";
import Button from "~/components/button";
import { Game, Player } from "~/types";

export default function Score() {
    const [players, setPlayers] = useState<Player[]>()
    const navigate = useNavigate()
    useEffect(() => {
        let game = sessionStorage.getItem('current_game')
        if(game){
            let game_json = JSON.parse(game)
            setPlayers([...game_json.players.sort(({score:a}, {score:b}) => b-a)])
        }
    }, [])

    const deleteGame = () => {
        sessionStorage.removeItem('current_game')
        sessionStorage.removeItem('player')
        sessionStorage.removeItem('gameId')
        navigate('/')
    }

    return (
        <>
        <ul className="w-full">
            {players && players.map((v,k) => (
                <li key={k} className={"p-2 flex justify-between items-center " + (k % 2 === 0 ? "bg-gray-200" : "bg-gray-300")}>
                    <span className="font-bold text-xl">{k+1} <span className="ml-2 font-normal">{v.name} </span></span>
                    <span>{v.score} {v.score === 1 ? 'point' : 'points'}</span>
                    
                </li>
            ))}
        </ul>
        <Button onClick={() => deleteGame} className="bg-red-500 hover:bg-red-700">Retour à l'accueil</Button>
        </>
    )
}