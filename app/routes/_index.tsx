import type { MetaFunction } from "@remix-run/node";
import { Link } from "@remix-run/react";
import { useEffect, useState } from "react";
import Button from "~/components/button";
import { socket } from "~/socket";

export const meta: MetaFunction = () => {
  
  return [
    { title: "Buzz home" },
    { name: "description", content: "Create or join a game" },
  ];
};

export default function Index() {

  useEffect(() => { 

}, [socket])

  return (
    <>
      <h1 className="text-center text-3xl font-bold text-orange-500">Buzzer together</h1>
        <Button className="bg-blue-500 hover:bg-blue-700 flex justify-center items-center" to={'/create'}>
          <span className="text-3xl mr-2">+</span> Nouvelle partie
        </Button>
        <Button className="bg-orange-500 hover:bg-orange-700" to={'/join'}>Rejoindre une partie</Button>
    </>
  );
}


