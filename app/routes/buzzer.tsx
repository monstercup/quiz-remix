import { Link } from "@remix-run/react"
import { useEffect, useState } from "react"
import { apiRequest } from "~/api"
import Button from "~/components/button"
import { socket } from "~/socket"
import { Game, Player } from "~/types"

export default function Buzzer() {
    const [game, setGame] = useState<Game | null>()
    const [queue, setQueue] = useState<Player[]>([])
    const [player, setPlayer] = useState<Player | null>()
    const [canBuzz, setCanBuzz] = useState<boolean>()
    const [buzzerStatus, setBuzzerStatus] = useState<string>()

    const buzz = () => {
        socket.emit('buzz', player)
        setCanBuzz(false)
        //edit player / receive player update
    }

    const getGame = () => {
        let id = sessionStorage.getItem('gameId')
        let name = sessionStorage.getItem('playerName')
        if(id){
            const url = "http://localhost:4000/game/" + id
            return apiRequest(url, "GET").then((res:any) => {
                if(res satisfies Game) {
                    if (res.players.find((p:Player) => p.name === name)){
                        const player = res.players.find((p:Player) => p.name === name)
                        setGame(res)
                        setPlayer(player)
                        setCanBuzz(player.canBuzz)
                        socket.emit('join-game', res)
                        // setGameStatus({msg:"En attente de démarrage de la partie", type:"info"})
                    }
                    else{
                        sessionStorage.removeItem(('gameId'))
                        setPlayer(null)
                    }                
                }
            })
        }
    }

    useEffect(() => {
        getGame()
        // const current_player = sessionStorage.getItem("player")
        // current_player && setPlayer(JSON.parse(current_player))
        // socket.emit('start_game', game?.id)
    }, [])

    useEffect(() => {
        socket.on('update_queue', (queue:Player[]) => {
            setQueue(queue ? queue : [])
            if(player && queue.length > 0){
                let status = 
                    queue[0].name === player.name 
                        ? "A vous de répondre !" 
                        : queue.filter((v:Player) => v.name === player.name) 
                            ? "C'est bientôt à votre tour de répondre.." 
                            : canBuzz 
                                ? ""
                                : "Vous ne pouvez pas buzzer pour l'instant !"
                console.log(status)
                setBuzzerStatus(status)
            }

        })
        // socket.on('queue_update', (p) => {
        //     if(p.name === player?.name) setPlayer(p)
        // })
    
        socket.on('cancelled', () => {
            setPlayer(null)
            setGame(null)
        })

        socket.on('game-update', game => {
            setGame(game)
            let name = sessionStorage.getItem('playerName')
            let player_updated = game.players.find((p:Player) => p.name === name)
            setPlayer(player_updated)
            setCanBuzz(player_updated.canBuzz)
        })
    }, [socket])

    if (player && game) return(
        <>
            <h1 className="text-3xl text-center mb-5 h-[75px]">{buzzerStatus}</h1>
            
            <Button className="!rounded-full w-[300px] h-[300px] !p-0 bg-red-600 hover:bg-red-700 text-white text-center text-6xl" disabled={!canBuzz} onClick={() => buzz()}>BUZZ</Button>
            
            <div className='text-center mt-5'>
                <span>{player.name}</span>
                <h2 className="text-3xl text-center font-bold">{player.score} point{player.score === 1 ? "" : "s"}</h2>
            </div>

        </>

    )
    else return (
        <>
            <p>Pas de joueur connecté</p>
            <Link to='/join' className="text-center underline hover:text-blue-600">Rejoindre une partie</Link>
        </>
        
    )
}
